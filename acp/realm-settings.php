<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>

        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-globe-americas"></i> Realm Settings</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-dark" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Realm Name</th>
                            <th>Public Addres</th>
                            <th>Local Address</th>
                            <th>Port</th>
                            <th>Icon</th>
                            <th>Flag</th>
                            <th>Timezone</th>
                            <th>Build</th>
                            <th>Region</th>
                            <th>Battlegroup</th>
                            <th>Tools</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Realm Name</th>
                            <th>Public Addres</th>
                            <th>Local Address</th>
                            <th>Port</th>
                            <th>Icon</th>
                            <th>Flag</th>
                            <th>Timezone</th>
                            <th>Build</th>
                            <th>Region</th>
                            <th>Battlegroup</th>
                            <th>Tools</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        $realm_query = $mysqliA->query("SELECT * FROM `realmlist`") or die (mysqli_error($mysqliA));
                        $num_query = $realm_query->num_rows;
                        if($num_query < 1)
                        {
                            echo '<tr><td colspan="6">There are no realms!</td></tr>';
                        }
                        else
                        {
                            while($res = $realm_query->fetch_assoc())
                            {
                                $realm_id = $res['id'];
                                $realm_name = $res['name'];
                                $realm_publicIP = $res['address'];
                                $realm_localIP = $res['localAddress'];
                                $realm_port = $res['port'];
                                $realm_a_icon = $res['icon'];
                                $realm_a_flag = $res['flag'];
                                $realm_a_timezone = $res['timezone'];
                                $realm_build = $res['gamebuild'];
                                $realm_a_region = $res['Region'];
                                $realm_battleg = $res['Battlegroup'];
                                switch ($realm_a_region){
                                    case 1:
                                        $realm_region = 'US';
                                        break;
                                    case 2:
                                        $realm_region = 'Europe';
                                        break;
                                    case 3:
                                        $realm_region = 'China';
                                        break;
                                    case 4:
                                        $realm_region = 'Korea';
                                        break;
                                }
                                switch($realm_a_icon){
                                    case 0:
                                        $realm_icon = 'Normal';
                                        break;
                                    case 1:
                                        $realm_icon = 'PvP';
                                        break;
                                    case 4:
                                        $realm_icon = 'Normal';
                                        break;
                                    case 6:
                                        $realm_icon = 'RP';
                                        break;
                                    case 8:
                                        $realm_icon = 'RP PvP';
                                        break;
                                }
                                switch ($realm_a_flag){
                                    case 0:
                                        $realm_flag = 'None';
                                        break;
                                    case 1:
                                        $realm_flag = 'Invalid';
                                        break;
                                    case 2:
                                        $realm_flag = 'Offline';
                                        break;
                                    case 4:
                                        $realm_flag = 'SpecifyBuild';
                                        break;
                                    case 8:
                                        $realm_flag = 'Medium';
                                        break;
                                    case 16:
                                        $realm_flag = 'Medium';
                                        break;
                                    case 32:
                                        $realm_flag = 'New Players';
                                        break;
                                    case 64:
                                        $realm_flag = 'Recommended';
                                        break;
                                    case 128:
                                        $realm_flag = 'Full';
                                }
                                switch ($realm_a_timezone){
                                    case 1:
                                        $realm_timezone = 'Development';
                                        break;
                                    case 2:
                                        $realm_timezone = 'United States';
                                        break;
                                    case 3:
                                        $realm_timezone = 'Oceanic';
                                        break;
                                    case 4:
                                        $realm_timezone = 'Latin America';
                                        break;
                                    case 5:
                                        $realm_timezone = 'Tournament';
                                        break;
                                    case 6:
                                        $realm_timezone = 'Korea';
                                        break;
                                    case 8:
                                        $realm_timezone = 'English';
                                        break;
                                    case 9:
                                        $realm_timezone = 'German';
                                        break;
                                    case 10:
                                        $realm_timezone = 'French';
                                        break;
                                    case 11:
                                        $realm_timezone = 'Spanish';
                                        break;
                                    case 12:
                                        $realm_timezone = 'Russian';
                                        break;
                                    case 14:
                                        $realm_timezone = 'Taiwan';
                                        break;
                                    case 16:
                                        $realm_timezone = 'China';
                                        break;
                                    case 26:
                                        $realm_timezone = 'Test Server';
                                        break;
                                    case 30:
                                        $realm_timezone = 'Test Server 2';
                                        break;
                                    case 37:
                                        $realm_timezone = 'Russian Tournament';
                                        break;
                                    case 49:
                                        $realm_timezone = 'Brazil';
                                        break;
                                    case 50:
                                        $realm_timezone = 'Italian';
                                        break;
                                    case 51:
                                        $realm_timezone = 'Hyrule';
                                        break;
                                    case 55:
                                        $realm_timezone = 'Recommended Realm';
                                        break;
                                }
                                echo '
                                    <tr>
                                       <td>'. $realm_id .'</td>
                                       <td>'. $realm_name .'</td>
                                       <td>'. $realm_publicIP .'</td>
                                       <td>'. $realm_localIP .'</td>
                                       <td>'. $realm_port .'</td>
                                       <td>'. $realm_icon .'</td>
                                       <td>'. $realm_flag .'</td>
                                       <td>'. $realm_timezone .'</td>
                                       <td>'. $realm_build .'</td>
                                       <td>'. $realm_region .'</td>
                                       <td>Battlegroup - '. $realm_battleg .'</td>
                                       <td><a href="'.$custdir.'/acp/realm-edit.php?id='. $realm_id .'" class="btn btn-sm btn-primary"><i class="fad fa-edit"></i> Edit this realm</a></td>
                                    </tr>
                                    ';
                            }
                        }

                        ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>