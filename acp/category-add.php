<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>

        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-plus-circle"></i> Add Category</div>
            <div class="card-body">
                <?php
                if(isset($_POST['addcat']))
                {
                    $catname = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['catname']));

                    if(empty($catname))
                    {
                        echo '
                            <div class="alert alert-warning" role="alert">
                              <i class="fad fa-exclamation-triangle"></i> Please check fields!
                            </div>
                         ';
                        header("refresh:3; url=$custdir/acp/category-add.php");
                    }
                    else
                    {
                        //let's check for duplicates
                        $cat_check = $mysqliA->query("SELECT * FROM `store_items_categorys` WHERE `name` = '$catname'") or die (mysqli_error($mysqliA));
                        $num_check = $cat_check->num_rows;
                        if($num_check > 0)
                        {
                            echo '
                                <div class="alert alert-warning" role="alert">
                                  <i class="fad fa-exclamation-triangle"></i> This category already exists
                                </div>
                             ';
                            header("refresh:3; url=$custdir/acp/store-categorys.php");
                        }
                        else
                        {
                            //insert
                            $cat_add = $mysqliA->query("INSERT INTO `store_items_categorys` (`name`) VALUES ('$catname')") or die (mysqli_error($mysqliA));
                            if($cat_add === true)
                            {
                                echo '
                                    <div class="alert alert-success" role="alert">
                                      <i class="fad fa-check-circle"></i> Category was added
                                    </div>
                                ';
                                header("refresh:3; url=$custdir/acp/store-categorys.php");
                            }
                        }
                    }

                }
                else
                {
                    ?>
                    <form name="addcat" method="post">
                        <div class="form-group">
                            <label for="catname">Category name</label>
                            <input type="text" name="catname" class="form-control" required="required">
                            <small>Name it whatever you want</small>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success" name="addcat"><i class="fad fa-plus-circle"></i> Add Category</button>
                        </div>
                    </form>
                    <?php
                }

                ?>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>